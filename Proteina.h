#ifndef LISTA_H
#define LISTA_H
#define TRUE 1
#define FALSE 0

#include <iostream>
#include<cstring>
using namespace std;



typedef struct _NODO {
  struct _NODO *izq;
  struct _NODO *der;
  string info;
  int FE;
} NODO;


class Proteina {
    private:
        NODO *raiz= NULL;
      
      

    public:
        /* constructor*/
        Proteina();
        void InsercionBalanceado(NODO **nodocabeza, int *BO, string infor);
        void Busqueda(NODO *nodo, string infor);
        void Restructura1(NODO **nodocabeza, int *BO);  
        void Restructura2(NODO **nodocabeza, int *BO);
        void Borrar(NODO **aux1, NODO **otro1, int *BO);
        void EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor);
        void Menu(int opc, string info);
        void GenerarGrafo(NODO *p);
        void PreOrden(NODO *, ofstream &fp);
        void Leer_archivo( string nombre);
           

      
};
#endif
