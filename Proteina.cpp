#include <iostream>
using namespace std;
#define TRUE 1
#define FALSE 0
#include <fstream>
#include "Proteina.h"
#include <string>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
Proteina::Proteina() {}

void Proteina::Menu(int opcion, string elemento){
    NODO *raiz = NULL;
    int inicio;
    switch(opcion) {
      case 1:
        inicio=FALSE;
        InsercionBalanceado(&raiz, &inicio, elemento);
        GenerarGrafo(raiz);
        break;

      case 2:
      
        Busqueda(raiz, elemento);
        break;
      
      case 3:
        
        inicio=FALSE;
        EliminacionBalanceado(&raiz, &inicio, elemento);
        GenerarGrafo(raiz);
        break;
      
      case 4:
        GenerarGrafo(raiz);
        break;
      case 5:
        inicio=FALSE;
        Leer_archivo(elemento);
        break;
      
      case 0: 
        exit(0);
    }
    
  
}

void Proteina::InsercionBalanceado (NODO **nodocabeza, int *BO, string infor) {
    NODO *nodo = NULL;
  NODO *nodo1 = NULL;
  NODO *nodo2 = NULL; 
  
  nodo = *nodocabeza;


  if (nodo != NULL) {
    
    if (strcmp(infor.c_str(),nodo->info.c_str())< 0) {
      cout << "entrar a izquierda\n"<<endl;
      InsercionBalanceado(&(nodo->izq),BO, infor);
      cout<<"insercion izquierda" <<endl;
      if(*BO == TRUE) {
        
        switch (nodo->FE) {
          case 1: 
            nodo->FE = 0;
            *BO = FALSE;
            break;
          
          case 0: 
            nodo->FE = -1;
            break;
     
          case -1: 
            /* reestructuración del árbol */
            nodo1 = nodo->izq;
            
            /* Rotacion II */
            if (nodo1->FE <= 0) { 
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
              nodo->FE = 0;
              nodo = nodo1;
            
            } else { 
              /* Rotacion ID */
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;
         
              if (nodo2->FE == -1)
                nodo->FE = 1;
              else
                nodo->FE =0;
        
              if (nodo2->FE == 1)
                nodo1->FE = -1;
              else
                nodo1->FE = 0;
              
              nodo = nodo2;
            }
            
            nodo->FE = 0;
            *BO = FALSE;
            break;
        }
      } 
      
    }else {
      
      if (strcmp(infor.c_str(),nodo->info.c_str()) > 0) {
        InsercionBalanceado(&(nodo->der), BO, infor);
        
        if (*BO == TRUE) {
          
          switch (nodo->FE) {
            
            case -1: 
              nodo->FE = 0;
              *BO = FALSE;
              break;
            
            case 0: 
              nodo->FE = 1;
              break;
      
            case 1: 
              /* reestructuración del árbol */
                nodo1 = nodo->der;
              
              if (nodo1->FE >= 0) { 
                /* Rotacion DD */
                nodo->der = nodo1->izq;
                nodo1->izq = nodo;
                nodo->FE = 0;
                nodo = nodo1;
                
              } else { 
                /* Rotacion DI */
                nodo2 = nodo1->izq;
                nodo->der = nodo2->izq;
                nodo2->izq = nodo;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;
                
                if (nodo2->FE == 1)
                  nodo->FE = -1;
                else
                  nodo->FE = 0;
       
                if (nodo2->FE == -1)
                  nodo1->FE = 1;
                else
                  nodo1->FE = 0;
   
                nodo = nodo2;
              }
              
              nodo->FE = 0;
              BO = FALSE;
              break;
          }
        }
      } else {
        cout << "El nodo ya se encuentra en el árbol\n";
        }
      }
  }else{
    
    nodo = (struct _NODO*) malloc (sizeof(NODO));
    nodo->izq = NULL;
    nodo->der = NULL;
    cout<<"errordespues\n";
    nodo->info = infor;
    nodo->FE = 0;
    *BO = TRUE;
 
  }
  
  *nodocabeza = nodo;
   cout<<"errordespues2\n";
  

}



void Proteina::Busqueda (NODO *nodo, string infor) {

    if (nodo != NULL) {

    if (strcmp(infor.c_str(),nodo->info.c_str()) < 0) {
      Busqueda(nodo->izq,infor);
    } else {
      if (strcmp(infor.c_str(),nodo->info.c_str()) > 0) {
        Busqueda(nodo->der,infor);
      } else {
        cout<<"El nodo SI se encuentra en el árbol\n";
      }
    }
  } else {
    cout<<"El nodo NO se encuentra en el árbol\n";
  }
  

}

void Proteina::Restructura1 (NODO **nodocabeza, int *BO) {

    NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case -1: 
        nodo->FE = 0;
        break;
      
      case 0: 
        nodo->FE = 1;
        *BO = FALSE;
        break;
   
    case 1: 
      /* reestructuracion del árbol */
      nodo1 = nodo->der;
      
      if (nodo1->FE >= 0) { 
        /* rotacion DD */
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;
        
        switch (nodo1->FE) {
          case 0: 
            nodo->FE = 1;
            nodo1->FE = -1;
            *BO = FALSE;
            break;
          case 1: 
            nodo->FE = 0;
            nodo1->FE = 0;
            *BO = FALSE;
            break;           
        }
        nodo = nodo1;
      } else { 
        /* Rotacion DI */
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;
       
        if (nodo2->FE == 1)
          nodo->FE = -1;
        else
          nodo->FE = 0;
        
        if (nodo2->FE == -1)
          nodo1->FE = 1;
        else
          nodo1->FE = 0;
       
        nodo = nodo2;
        nodo2->FE = 0;       
      } 
      break;   
    }
  }
  *nodocabeza=nodo;
  

}
void Proteina::Restructura2 (NODO **nodocabeza, int *BO) {

    NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case 1: 
        nodo->FE = 0;
        break;
      case 0: 
        nodo->FE = -1;
        *BO = FALSE;
        break;
      case -1: 
        /* reestructuracion del árbol */
        nodo1 = nodo->izq;
        if (nodo1->FE<=0) { 
          /* rotacion II */
          nodo->izq = nodo1->der;
          nodo1->der = nodo;
          switch (nodo1->FE) {
            case 0: 
              nodo->FE = -1;
              nodo1->FE = 1;
              *BO = FALSE;
              break;
            case -1: 
              nodo->FE = 0;
              nodo1->FE = 0;
              *BO = FALSE;
              break;
          }
          nodo = nodo1;
        } else { 
          /* Rotacion ID */
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;
       
          if (nodo2->FE == -1)
            nodo->FE = 1;
          else
            nodo->FE = 0;
        
          if (nodo2->FE == 1)
            nodo1->FE = -1;
          else
            nodo1->FE = 0;
       
          nodo = nodo2;
          nodo2->FE = 0;       
        }      
        break;   
    }
  }
  *nodocabeza = nodo;
  

}
void Proteina::Borrar (NODO **aux1, NODO **otro1, int *BO) {

     NODO *aux, *otro; 
  aux=*aux1;
  otro=*otro1;
  
  if (aux->der != NULL) {
    Borrar(&(aux->der),&otro,BO); 
    Restructura2(&aux,BO);
  } else {
    otro->info = aux->info;
    aux = aux->izq;
    *BO = TRUE;
  }
  *aux1=aux;
  *otro1=otro;
  

}
void Proteina::EliminacionBalanceado (NODO **nodocabeza, int *BO, string infor) {

    NODO *nodo, *otro; 
  
  nodo = *nodocabeza;
  
  if (nodo != NULL) {

    if (strcmp(infor.c_str(),nodo->info.c_str()) < 0) {
      EliminacionBalanceado(&(nodo->izq),BO,infor);
      Restructura1(&nodo,BO);
    } else {
      if (strcmp(infor.c_str(),nodo->info.c_str())> 0) {
        EliminacionBalanceado(&(nodo->der),BO,infor);
        Restructura2(&nodo,BO); 
      } else {
        otro = nodo;
        if (otro->der == NULL) {
          nodo = otro->izq;
          *BO = TRUE;     
        } else {
          if (otro->izq==NULL) {
            nodo=otro->der;
            *BO=TRUE;     
          } else {
            Borrar(&(otro->izq),&otro,BO);
            Restructura1(&nodo,BO);
            free(otro);
          }
        }
      }
    } 
  } else {
    cout <<"El nodo NO se encuentra en el árbol\n";
  }
  *nodocabeza=nodo;
  

}
void Proteina::GenerarGrafo (NODO *ArbolInt) {

  ofstream fp;
  fp.open ("grafo.txt");
  
  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=yellow];" << endl;
  
 
  fp <<"nullraiz [shape=point];"<<endl;
  fp <<"nullraiz->"<< ArbolInt->info << "[label="<< ArbolInt->FE << "];"<<endl; 
  PreOrden(ArbolInt, fp);
  
  fp<< "}"<<endl;
  fp.close();
  
  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
  

}
void Proteina::PreOrden(NODO *a, ofstream &fp ){

 char cadena[16];
  
  if (a != NULL) {
    if (a->izq != NULL) {
   
      fp << "\n"<< '"' << a->info <<'"'<<"->"<<'"' << a->izq->info << '"'<<"[label="<<a->izq->FE<<"];"<<endl;
    } else{
     
      fp << "\n"<<'"'<< a->info << "i"<< '"'<<"[shape=point];";
      fp << "\n"<< a->info << "->"<<'"'<< a->info<<"i"<<'"';
    }
    
    if (a->der != NULL) {
       
      fp << "\n"<< '"' << a->info <<'"'<<"->"<<'"' << a->der->info << '"'<<"[label="<<a->der->FE<<"];"<<endl;
    } else{
      fp << "\n"<<'"'<< a->info << "d"<< '"'<<"[shape=point];";
      fp << "\n"<< a->info << "->"<<'"'<< a->info<<"d"<<'"';
    }

    PreOrden(a->izq,fp);
    PreOrden(a->der,fp); 
  }
  

}
void Proteina::Leer_archivo(string nombre){
 NODO *raiz = NULL; 
 string s;

 ifstream f( nombre.c_str() );

int inicio;
 if ( f.is_open() ) {
 getline( f, s );

 while( !f.eof() ) {
  inicio=FALSE;
 InsercionBalanceado(&raiz, &inicio, s);
 GenerarGrafo(raiz);   
 cout << s << endl;
 getline( f, s );
 
 }
 }
 else cerr << "Error de apertura del archivo." << endl;
}





