prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = avl.cpp Proteina.cpp
OBJ = avl.o Proteina.o
APP = avl

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)


