#include <iostream>
#include <fstream>
/*#include <stdlib.h>
#include <stdio.h>*/
#include <string>
#include <cstring>
#include "Proteina.h"

using namespace std;


int menu() {
  int Op;
  
  do {
    cout<<"\n--------------------\n";
    cout<<"1) Insertar\n";
    cout<<"2) Buscar\n";
    cout<<"3) Eliminacion\n";
    cout<<"4) Grafo\n";
    cout<<"5) Leer archivo\n";
    cout<<"0) Salir\n\n";
    cout<<"Opción: ";
    cin>>Op;
  } while (Op<0 || Op>7);
  
  return Op;
}


int main(int argc, char **argv) {
  int opcion;
  string elemento;
  NODO *raiz = NULL;
  Proteina *proteina = new Proteina();
  
  system("clear");
  opcion = menu();

  
  while (opcion) {
    
    switch(opcion) {
      case 1:
        cout<<"Ingresar elemento: ";
        cin >> elemento;
        
        proteina->Menu(opcion,elemento);
        break;

      case 2:
        cout <<"Buscar elemento: ";
        cin >> elemento;
        proteina->Menu(opcion,elemento);
        break;
      
      case 3:
        cout << "Eliminar elemento: ";
        cin >> elemento;
        proteina->Menu(opcion,elemento);
        break;
      
      case 4:
        proteina->Menu(opcion,elemento);
        break;

      case 5:
        cout << "Abrir archivo: ";
        cin >> elemento;
        proteina->Menu(opcion,elemento);
        break;
      
      case 0: 
        exit(0);
    }
    
    opcion = menu();
  }
  
  return 0;
}




